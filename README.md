# Para clonar este repositorio
1. Crea la carpeta donde vas a trabajar, la mia se llama "clases_python".
2. Muevete ahí y ejecuta "git clone https://toodivz@bitbucket.org/toodivz/clases_python.git"
3. Se va a descargar todo lo que tenga el repositorio del proyecto.
4. Si vas a modificar varios archivos usa "git add ." el punto incluye en el versionador 
a todos los archivos dentro del directorio.
5. Cada que quieras guardar algo relevante usas "git commit -m "algún mensaje relevante"" 
y eso guardara la versión.
6. Para subir estos cambios de nuevo a bitbucket usas el comando "git push"
7. Para descargar las modificaciones que hayamos hecho alguno ejecutar "git pull"